package com.example.ledbox.weather;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ledbox on 01.09.2018.
 */

public class SomeMethods {
    public static String getFormattedTime(long fTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM yyyy, EEEE, k:m");
        return simpleDateFormat.format(new Date(fTime));

    }

}
