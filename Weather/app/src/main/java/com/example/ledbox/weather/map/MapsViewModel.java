package com.example.ledbox.weather.map;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.example.ledbox.weather.AppSettings;
import com.example.ledbox.weather.Constants;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Ledbox on 23.08.2018.
 */

public class MapsViewModel extends AndroidViewModel {
    private double dLatitude=0,dLongtitude=0;
    private String sMarkerTitle=null;
    private AppSettings appSettings;
    private boolean isHaveMarker=false;
    boolean NEED_LOG = Constants.NEED_LOG;
    String TAG = "MapsActivity";

    public MapsViewModel(@NonNull Application application) {
        super(application);
        appSettings=new AppSettings(application.getBaseContext());
        if(appSettings.iaHaveLastPlace()){
            dLatitude=Double.valueOf(appSettings.getLastLatitude());
            dLongtitude=Double.valueOf(appSettings.getLastLongtitude());
            sMarkerTitle=appSettings.getLastCity();
            isHaveMarker=true;
        }
    }

    public double getdLatitude() {
        return dLatitude;
    }

    public double getdLongtitude() {
        return dLongtitude;
    }

    public String getsMarkerTitle() {
        return sMarkerTitle;
    }

    public void setMarkerState(Marker marker) {
        dLatitude = marker.getPosition().latitude;
        dLongtitude=marker.getPosition().longitude;
        sMarkerTitle=marker.getTitle();
        isHaveMarker=true;
    }

    public boolean isHaveMarker() {
        return isHaveMarker;
    }




}
