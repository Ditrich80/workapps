package com.example.ledbox.weather.places;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.ledbox.weather.databinding.ItemRecyclerPlacesBinding;
import java.util.List;


public class AdapterRecyclerPlaces extends RecyclerView.Adapter<AdapterRecyclerPlaces.PlacesViewHolder> {
    private LayoutInflater layoutInflater;
    private List<Place>placeList;
    private PlacesClickListener listener;



    public AdapterRecyclerPlaces(Context context,PlacesClickListener listener) {
        this.listener = listener;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PlacesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRecyclerPlacesBinding binding=ItemRecyclerPlacesBinding.inflate(layoutInflater,parent,false);
        return new PlacesViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewHolder holder, int position) {
        if(placeList==null||placeList.size()==0)return;
        final Place place=placeList.get(position);
        holder.binding.setPlace(place);
        holder.binding.setClick(listener);
    }

    public Place getItemByPos(int position) {
        return placeList.get(position);
    }


    @Override
    public int getItemCount() {
        return placeList==null?0:placeList.size();
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder{
        ItemRecyclerPlacesBinding binding;
        public PlacesViewHolder(View itemView) {
            super(itemView);
            binding= DataBindingUtil.bind(itemView);
        }
    }

    public void setPlaceList(List<Place> placeList) {
        this.placeList = placeList;
        notifyDataSetChanged();
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String sPath){
        Glide.with(imageView.getContext()).load(sPath).into(imageView);
    }
}


