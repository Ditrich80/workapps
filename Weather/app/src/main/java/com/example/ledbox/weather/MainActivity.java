package com.example.ledbox.weather;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.example.ledbox.weather.map.MapsActivity;
import com.example.ledbox.weather.places.PlacesListFragment;
import com.example.ledbox.weather.places.Place;
import com.example.ledbox.weather.weathers.WeatherActivity;

public class MainActivity extends AppCompatActivity{
    private static final int REQUEST_CODE_MAPP_ACTIVITY = 10000;
    private static final int REQUEST_CODE_WEATHER_ACTIVITY = 10001;
    boolean NEED_LOG = Constants.NEED_LOG;
    String TAG = "MapsActivity";
    AppSettings appSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        init();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case REQUEST_CODE_MAPP_ACTIVITY:
                if(NEED_LOG)Log.d(TAG,"onActivityResult");
                break;
        }
    }


    public void onItemInAdapterPlacesClick(Place place) {
        if(NEED_LOG)Log.d(TAG,"onItemInAdapterPlacesClick "+place.sName);
        Intent intent=new Intent(getBaseContext(),WeatherActivity.class);
        intent.putExtra(Constants.LOCATION_LATITUDE_DATA,place.dLatitude);
        intent.putExtra(Constants.LOCATION_LONGITUDE_DATA,place.dLongtitude);
        intent.putExtra(Constants.LOCATION_PLACE,place.sName);
        intent.putExtra(Constants.PARENT_ID,place.id);
        startActivityForResult(intent,REQUEST_CODE_WEATHER_ACTIVITY);
    }


    public void onIconPressed(Place place) {
        if(NEED_LOG)Log.d(TAG,"onIconPressed "+place.sName);
        appSettings.setCameraState(place.getCameraPosition(),null);
        appSettings.setLastLatitude(place.dLatitude);
        appSettings.setLastLongtitude(place.dLongtitude);
        appSettings.setLastCity(place.sName);
        Intent intent=new Intent(getBaseContext(),MapsActivity.class);
        startActivityForResult(intent,REQUEST_CODE_MAPP_ACTIVITY);
    }

    private void init(){
        appSettings=new AppSettings(getBaseContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.places));
        toolbar.setNavigationIcon(R.drawable.ic_place_white);
        setSupportActionBar(toolbar);
        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getBaseContext(),MapsActivity.class);
                startActivityForResult(intent,REQUEST_CODE_MAPP_ACTIVITY);
            }
        });
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentPlacesMain, PlacesListFragment.newInstance()).commitNow();
    }

}
