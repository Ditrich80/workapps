package com.example.ledbox.weather.weathers;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;

import com.example.ledbox.weather.Constants;

public class WeatherViewModel extends ViewModel {
    private double dLatitude=0, dLongtitude=0;
    private MutableLiveData<String> sPlace=new MutableLiveData<>();
    private boolean HaveInfo=false;

    public void setData(Intent incomingIntent) {
        this.dLatitude = incomingIntent.getDoubleExtra(Constants.LOCATION_LATITUDE_DATA,0);
        this.dLongtitude = incomingIntent.getDoubleExtra(Constants.LOCATION_LONGITUDE_DATA,0);
        this.sPlace.setValue(incomingIntent.getStringExtra(Constants.LOCATION_PLACE).toUpperCase());
        HaveInfo=true;
    }

    public MutableLiveData<String> getsPlace() {
        return sPlace;
    }

    public double getdLatitude() {
        return dLatitude;
    }

    public double getdLongtitude() {
        return dLongtitude;
    }

    public boolean isHaveInfo() {
        return HaveInfo;
    }
}
