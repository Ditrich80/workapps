package com.example.ledbox.weather.places;

import com.example.ledbox.weather.places.Place;

/**
 * Created by Ledbox on 01.09.2018.
 */

public interface PlacesClickListener {
    public void onItemInAdapterPlacesClick(Place place);
    public void onIconPressed(Place place);
}
