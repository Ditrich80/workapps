package com.example.ledbox.weather.weathers;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Ledbox on 22.08.2018.
 */

public class WeatherDay {
    String TAG = "MapsActivity";
    public class WeatherTemp {
        Double temp;
        Double temp_min;
        Double temp_max;

    }

    public class Coord{
        Double lat;
        Double lon;

    }

    public class WeatherDescription {
        String description;
        String icon;
    }

    public void makeLog(){

        Log.d(TAG,"lat="+coord.lat);
        Log.d(TAG,"lon="+coord.lon);
        Log.d(TAG,"Temp="+temp.temp);
        Log.d(TAG,"TempMax="+temp.temp_max);
        Log.d(TAG,"TempMin="+temp.temp_min);
        Log.d(TAG,"description="+arrDesctiption.get(0).description);
        Log.d(TAG,"Date="+getDate() );
        Log.d(TAG,"Icon="+arrDesctiption.get(0).icon);
    }

    @SerializedName("main")
    private WeatherTemp temp;

    @SerializedName("coord")
    private Coord coord;

    @SerializedName("weather")
    private List<WeatherDescription> arrDesctiption;


    @SerializedName("dt")
    private long dt;

    public WeatherDay(WeatherTemp temp, List<WeatherDescription> desctiption) {
        this.temp = temp;
        this.arrDesctiption = desctiption;
    }

    public String getDate() {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("d MMMM yyyy, EEEE, k:m");
        return simpleDateFormat.format(new Date(dt));
    }

    public String getDescription() {
        return arrDesctiption.get(0).description;
    }

    public Double getTemp() { return temp.temp; }

    public Double getTempMin() { return temp.temp_min; }

    public Double getTempMax() { return temp.temp_max; }

    public String getTempInteger() { return String.valueOf(temp.temp.intValue()); }

    public String getTempWithDegree() { return String.valueOf(temp.temp.intValue()) + "\u00B0"; }

    public String getIcon() { return arrDesctiption.get(0).icon; }

    public String getIconUrl() {
        return "http://openweathermap.org/img/w/" + arrDesctiption.get(0).icon + ".png";
    }
}
