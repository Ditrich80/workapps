package com.example.ledbox.weather.places;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ledbox.weather.Constants;
import com.example.ledbox.weather.R;
import com.example.ledbox.weather.MainActivity;

import java.util.List;

/**
 * Created by Ledbox on 02.09.2018.
 */

public class PlacesListFragment extends android.support.v4.app.Fragment implements PlacesClickListener, AdapterPlaceListener {
    private boolean NEED_LOG = Constants.NEED_LOG;
    private String TAG = "MapsActivity";
    private RecyclerView recyclerView;
    private AdapterRecyclerPlaces adapterRecyclerPlaces;
    private PlacesViewModel placesViewModel;
    private Context context;
    public static PlacesListFragment newInstance(){
        return new PlacesListFragment();
    }
    @Override
    public void onAttach(Context context) {
        if(NEED_LOG)Log.d(TAG,"PlacesListFragment onAttach");
        super.onAttach(context);
        this.context=context;
        adapterRecyclerPlaces=new AdapterRecyclerPlaces(context,this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if(NEED_LOG)Log.d(TAG,"PlacesListFragment onCreate");
        super.onCreate(savedInstanceState);
        init();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(NEED_LOG) Log.d(TAG,"PlacesListFragment onCreateView");
        View view = inflater.inflate(R.layout.fragment_places, container, false);
        recyclerView=view.findViewById(R.id.recyclerMainPlaces);
        recyclerView.setAdapter(adapterRecyclerPlaces);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        ItemTouchHelper.Callback callbackBL = new ItemTouchHelperCallbackPlaces(this);
        ItemTouchHelper mItemTouchHelperBL = new ItemTouchHelper(callbackBL);
        mItemTouchHelperBL.attachToRecyclerView(recyclerView);
        return view;
    }

    @Override
    public void onItemInAdapterPlacesClick(Place place) {
        ((MainActivity)getActivity()).onItemInAdapterPlacesClick(place);
    }

    @Override
    public void onIconPressed(Place place) {
        ((MainActivity)getActivity()).onIconPressed(place);
    }

    @Override
    public void onAdapterDeleteAction(int pos) {
        final Place place=adapterRecyclerPlaces.getItemByPos(pos);
        AlertDialog.Builder builderDel = new AlertDialog.Builder(context);
        builderDel.setMessage(getString(R.string.delete_item)+" \""+place.sName+"\"?");
        builderDel.setIcon(R.drawable.ic_delete_black_24dp);
        builderDel.setTitle(getString(R.string.delete_item));
        builderDel.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                placesViewModel.delPlaceByID(place.id);
            }
        });
        builderDel.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                adapterRecyclerPlaces.notifyDataSetChanged();
            }
        });
        builderDel.setCancelable(true);
        AlertDialog alertDialog = builderDel.create();
        alertDialog.show();
    }

    private void init(){
        placesViewModel= ViewModelProviders.of(this).get(PlacesViewModel.class);
        placesViewModel.getPlaceLiveData().observe( this, new Observer<List<Place>>() {
            @Override
            public void onChanged(@Nullable List<Place> places) {
                adapterRecyclerPlaces.setPlaceList(places);
            }
        });

    }


}
