package com.example.ledbox.weather.weathers;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.example.ledbox.weather.places.Place;

/**
 * Created by Ledbox on 23.08.2018.
 */
@Entity(tableName = "weather_data",
        foreignKeys = @ForeignKey(entity = Place.class,
                parentColumns = "p_id",
                childColumns = "parentid",
                onDelete = ForeignKey.CASCADE)
        ,indices = {@Index("parentid"), @Index("temp"), @Index("temp_min")
        , @Index("temp_max"), @Index("insert_time"), @Index("description"), @Index("icon")}
        )
public class WeatherItem {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "w_id")
    public int id;

    @ColumnInfo(name = "parentid")
    public int parentid;

    @ColumnInfo(name = "temp")
    public double temp;

    @ColumnInfo(name = "temp_min")
    public double temp_min;

    @ColumnInfo(name = "temp_max")
    public double temp_max;

    @ColumnInfo(name = "insert_time")
    public long insert_time;

    @ColumnInfo(name = "description")
    public String description;

    @ColumnInfo(name = "icon")
    public String icon;

    public WeatherItem(int parentid, double temp, double temp_min, double temp_max, String description, String icon) {
        this.parentid = parentid;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.description = description;
        this.icon = icon;
    }
    public WeatherItem(WeatherDay weatherDay,int nParentID,long lTime) {
        this.parentid = nParentID;
        this.temp = weatherDay.getTemp();
        this.temp_min = weatherDay.getTempMin();
        this.temp_max = weatherDay.getTempMax();
        this.description = weatherDay.getDescription();
        this.icon = weatherDay.getIconUrl();
        this.insert_time=lTime;
    }
    public String getTemp() {
        return doubleToStringInt(temp);
    }

    public String getTemp_min() {
        return doubleToStringInt(temp_min);
    }

    public String getTemp_max() {
        return doubleToStringInt(temp_max);
    }

    private String doubleToStringInt(double dNum){
        return String.valueOf(Math.round(dNum))+ "\u00B0";
    }


}
