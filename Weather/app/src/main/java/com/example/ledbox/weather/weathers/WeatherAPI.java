package com.example.ledbox.weather.weathers;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ledbox on 22.08.2018.
 */

public class WeatherAPI {
    public static String KEY = "55fd05fc3d83f775b97765abc8079fb2";
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private static Retrofit retrofit = null;

    public interface ApiInterface {
        @GET("weather")
        Call<WeatherDay> getToday(
                @Query("lat") Double dLatitude,
                @Query("lon") Double dLongtitude,
                @Query("units") String sUnits,
                @Query("appid") String app_ID,
                @Query("lang") String sLanguage
        );


    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
