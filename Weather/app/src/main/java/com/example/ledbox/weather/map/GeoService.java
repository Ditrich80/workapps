package com.example.ledbox.weather.map;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.ledbox.weather.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ledbox on 22.08.2018.
 */

public class GeoService extends IntentService {
    protected ResultReceiver resultReceiver;
    boolean NEED_LOG= Constants.NEED_LOG;
    String TAG="MapsActivity";


    public GeoService() {
        super("GeoService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(NEED_LOG) Log.d(TAG,"GeoService onHandleIntent");
        String sError="";
        if(intent==null)return;
        double dLatitude=intent.getDoubleExtra(Constants.LOCATION_LATITUDE_DATA,0);
        double dLongitude=intent.getDoubleExtra(Constants.LOCATION_LONGITUDE_DATA,0);
        resultReceiver=intent.getParcelableExtra(Constants.RECEIVER);
        if(NEED_LOG) Log.d(TAG,"GeoService Locale="+getResources().getConfiguration().locale);
        Geocoder geocoder = new Geocoder(this, getResources().getConfiguration().locale);
        List<Address> addresses = null;
        try {
           addresses = geocoder.getFromLocation(dLatitude, dLongitude, 1);
          //  addresses=geocoder.getFromLocationName()
        } catch (IOException ioException) {
            sError="Service Not Available";
            Log.e(TAG,sError , ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            sError="Invalid Latitude or Longitude Used";
            Log.e(TAG,  sError+ ". " +
                    "Latitude = " + dLatitude + ", Longitude = " +
                    dLongitude, illegalArgumentException);
        }
        if(addresses==null||addresses.size()==0){
            if(sError.isEmpty()){
                sError="Place not found";
                Log.e(TAG,sError);
            }
            deliverResultToReceiver(Constants.FAILURE_RESULT, sError, null);
        }else deliverResultToReceiver(Constants.SUCCESS_RESULT, "Fouded place", addresses.get(0));

    }
    private void deliverResultToReceiver(int resultCode, String message, Address address) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.RESULT_ADDRESS, address);
        bundle.putString(Constants.RESULT_MESSAGE, message);
        resultReceiver.send(resultCode, bundle);
    }
}
