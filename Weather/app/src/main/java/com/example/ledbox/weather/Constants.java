package com.example.ledbox.weather;

/**
 * Created by Ledbox on 22.08.2018.
 */

public final class Constants {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final boolean NEED_LOG = false;

    public static final String RECEIVER = "RECEIVER";
    public static final String LOCATION_LATITUDE_DATA = "LOCATION_LATITUDE_DATA";
    public static final String LOCATION_LONGITUDE_DATA = "LOCATION_LONGITUDE_DATA";
    public static final String LOCATION_PLACE = "LOCATION_PLACE";
    public static final String PARENT_ID = "PARENT_ID";
    public static final String RESULT_ADDRESS = "RESULT_ADDRESS";
    public static final String RESULT_MESSAGE = "RESULT_MESSAGE";

}
