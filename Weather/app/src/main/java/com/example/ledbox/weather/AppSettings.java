package com.example.ledbox.weather;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Ledbox on 22.08.2018.
 */

public class AppSettings {
    private SharedPreferences m_SharedPreferences;
    private static final String KEY_LAST_LATITUDE= "KEY_LAST_LATITUDE";
    private static final String KEY_LAST_LONGTITUDE= "KEY_LAST_LONGTITUDE";
    private static final String KEY_LAST_CITY= "KEY_LAST_CITY";
    private static final String KEY_CAMERA_LATITUDE= "KEY_CAMERA_LATITUDE";
    private static final String KEY_CAMERA_LONGTITUDE= "KEY_CAMERA_LONGTITUDE";
    private static final String KEY_CAMERA_ZOOM= "KEY_CAMERA_ZOOM";
    private static final String KEY_CAMERA_BEARING= "KEY_CAMERA_BEARING";
    private static final String KEY_CAMERA_TILT= "KEY_CAMERA_TILT";
    private static final String KEY_CAMERA_HEAVE_STATE= "KEY_CAMERA_HEAVE_STATE";
    public AppSettings(Context context) {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setLastMarkerState(Marker marker){
        setLastLatitude(marker.getPosition().latitude);
        setLastLongtitude(marker.getPosition().longitude);
        setLastCity(marker.getTitle());
    }

    public void setCameraState(CameraPosition position,Marker marker){
        setCameraLatitude(marker==null?position.target.latitude:marker.getPosition().latitude);
        setCameraLongtitude(marker==null?position.target.longitude:marker.getPosition().longitude);
        setCameraZoom(position.zoom);
        setCameraBearing(position.bearing);
        setCameraTilt(position.tilt);
        setCameraHaveLastState();
    }



    public CameraPosition getCameraLastPosition(){
        if(!isHaveCameraLastState())return null;
        LatLng latLng=new LatLng(getCameraLatitude(),getCameraLongtitude());
        CameraPosition position=new CameraPosition(latLng
        ,getCameraZoom(),getCameraTilt(),getCameraBearing());
        return position;
    }

    public boolean isHaveCameraLastState(){
        return m_SharedPreferences.getBoolean(KEY_CAMERA_HEAVE_STATE, false);
    }

    public void setCameraHaveLastState(){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_CAMERA_HEAVE_STATE, true);
        editor.commit();
    }

    public float getCameraTilt(){
        return m_SharedPreferences.getFloat(KEY_CAMERA_TILT, 0f);
    }

    public void setCameraTilt(float fTilt){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putFloat(KEY_CAMERA_TILT, fTilt);
        editor.commit();
    }

    public float getCameraBearing(){
        return m_SharedPreferences.getFloat(KEY_CAMERA_BEARING, 0f);
    }

    public void setCameraBearing(float fBearing){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putFloat(KEY_CAMERA_BEARING, fBearing);
        editor.commit();
    }

    public float getCameraZoom(){
        return m_SharedPreferences.getFloat(KEY_CAMERA_ZOOM, 0f);
    }

    public void setCameraZoom(float fZoom){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putFloat(KEY_CAMERA_ZOOM, fZoom);
        editor.commit();
    }

    public double getCameraLongtitude(){
        return Double.valueOf(m_SharedPreferences.getString(KEY_CAMERA_LONGTITUDE, null));
    }

    public void setCameraLongtitude(double dLongtitude){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putString(KEY_CAMERA_LONGTITUDE, String.valueOf(dLongtitude));
        editor.commit();
    }

    public double getCameraLatitude(){
        return Double.valueOf(m_SharedPreferences.getString(KEY_CAMERA_LATITUDE, null));
    }

    public void setCameraLatitude(double dLatitude){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putString(KEY_CAMERA_LATITUDE, String.valueOf(dLatitude));
        editor.commit();
    }


    public boolean iaHaveLastPlace(){
        return getLastLatitude()!=null&&getLastLongtitude()!=null;
    }

    public String getLastCity(){
        return m_SharedPreferences.getString(KEY_LAST_CITY, null);
    }

    public void setLastCity(String sCity){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putString(KEY_LAST_CITY, sCity);
        editor.commit();
    }

    public String getLastLatitude(){
        return m_SharedPreferences.getString(KEY_LAST_LATITUDE, null);
    }

    public void setLastLatitude(double sLatitude){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putString(KEY_LAST_LATITUDE, String.valueOf(sLatitude));
        editor.commit();
    }

    public String getLastLongtitude(){
        return m_SharedPreferences.getString(KEY_LAST_LONGTITUDE, null);
    }

    public void setLastLongtitude(double sLongtitude){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putString(KEY_LAST_LONGTITUDE, String.valueOf(sLongtitude));
        editor.commit();
    }



}
