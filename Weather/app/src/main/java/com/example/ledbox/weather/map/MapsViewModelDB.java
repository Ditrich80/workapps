package com.example.ledbox.weather.map;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.ledbox.weather.Constants;
import com.example.ledbox.weather.places.Place;
import com.example.ledbox.weather.places.PlacesDao;
import com.example.ledbox.weather.WeatherDatabase;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Ledbox on 02.09.2018.
 */

public class MapsViewModelDB extends AndroidViewModel {
    private PlacesDao placesDao;
    private MutableLiveData<Boolean> Done=new MutableLiveData<>();
    boolean NEED_LOG = Constants.NEED_LOG;
    String TAG = "MapsActivity";
    public MapsViewModelDB(@NonNull Application application) {
        super(application);
        placesDao= WeatherDatabase.getDatabase(application).placesDao();
        Done.setValue(false);
    }

    public MutableLiveData<Boolean> getDone() {
        return Done;
    }

    public void insert(Marker marker, CameraPosition cameraPosition){
        if(NEED_LOG) Log.d(TAG,"insert Place"+marker.getTitle());
        Place place=new Place(marker,cameraPosition,System.currentTimeMillis());
        AsyncInserter asyncInserter=new AsyncInserter();
        asyncInserter.execute(place);
    }



    private class AsyncInserter extends AsyncTask<Place,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(Place... places) {
            if(NEED_LOG) Log.d(TAG,"doInBackground Place"+places[0].sName);
            placesDao.insert(places[0]);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Done.setValue(true);
        }
    }

}
