package com.example.ledbox.weather.places;

/**
 * Created by Ledbox on 24.08.2018.
 */

public interface AdapterPlaceListener {
    void onAdapterDeleteAction(int pos);
}
