package com.example.ledbox.weather.weathers;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.ledbox.weather.Constants;
import com.example.ledbox.weather.places.Place;
import com.example.ledbox.weather.places.PlacesDao;
import com.example.ledbox.weather.WeatherDatabase;

import java.util.List;

/**
 * Created by Ledbox on 02.09.2018.
 */

public class WeatherViewModelDB extends AndroidViewModel {
    private PlacesDao placesDao;
    private WeatherDao weatherDao;
    private LiveData<List<WeatherItem>> weatherItems;
    private int nParentID=-1;
    private Place parentPlace;
    private boolean NEED_LOG = Constants.NEED_LOG;
    private String TAG = "MapsActivity";

    public void setData(Intent incomingIntent) {
        this.nParentID=incomingIntent.getIntExtra(Constants.PARENT_ID,-1);
        weatherItems=weatherDao.getAllWeathersByID(nParentID);
        AsyncFounder asyncFounder=new AsyncFounder();
        asyncFounder.execute(nParentID);
    }

    public WeatherViewModelDB(@NonNull Application application) {
        super(application);
        placesDao= WeatherDatabase.getDatabase(application).placesDao();
        weatherDao= WeatherDatabase.getDatabase(application).weatherDao();

    }

    public LiveData<List<WeatherItem>> getWeatherItems() {
        return weatherItems;
    }

    public void insert(WeatherItem weatherItem){
        AsyncInserter asyncInserter=new AsyncInserter();
        asyncInserter.execute(weatherItem);
    }

    public void delWeatherItem(WeatherItem weatherItem){
        AsyncDeleter asyncDeleter=new AsyncDeleter();
        asyncDeleter.execute(weatherItem);
    }

    private class AsyncFounder extends AsyncTask<Integer,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(Integer... nIDs) {
            parentPlace=placesDao.findPlaceByID(nIDs[0]);
            return true;
        }
    }

    private class AsyncInserter extends AsyncTask<WeatherItem,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(WeatherItem... weatherItems) {
            weatherDao.insert(weatherItems[0]);
            parentPlace.nItemsColl=weatherDao.getAllWeathersByIDNoLiveData(nParentID).size();
            parentPlace.lLastUpdateDate=weatherItems[0].insert_time;
            placesDao.update(parentPlace);
            return true;
        }
    }

    private class AsyncDeleter extends AsyncTask<WeatherItem,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(WeatherItem... incomingWeatherItems) {
            weatherDao.deleteWeatherItemByID(incomingWeatherItems[0].id);
            List<WeatherItem>weatherItms=weatherDao.getAllWeathersByIDNoLiveData(nParentID);
            parentPlace.nItemsColl=weatherItms.size();
            parentPlace.lLastUpdateDate=weatherItms.size()==0?0:weatherItms.get(0).insert_time;
            weatherItms.clear();
            placesDao.update(parentPlace);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }

    public int getnParentID() {
        return nParentID;
    }


}
