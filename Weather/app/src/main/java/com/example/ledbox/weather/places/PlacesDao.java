package com.example.ledbox.weather.places;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.ledbox.weather.places.Place;

import java.util.List;

/**
 * Created by Ledbox on 23.08.2018.
 */
@Dao
public interface PlacesDao {

    @Query("SELECT * FROM places WHERE p_id = :id LIMIT 1")
    Place findPlaceByID(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Place place);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Place place);

    @Query("DELETE FROM places WHERE name = :sName")
    void deletePlaceByName(String sName);

    @Query("SELECT * FROM places ORDER BY creation_time DESC")
    LiveData<List<Place>>getAllPlaces();

}
