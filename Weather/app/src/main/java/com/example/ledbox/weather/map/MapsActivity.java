package com.example.ledbox.weather.map;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.location.Address;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import com.example.ledbox.weather.AppSettings;
import com.example.ledbox.weather.Constants;
import com.example.ledbox.weather.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
        , GoogleMap.OnMapClickListener {

    private PlaceResultReciever resultReciever;
    private GoogleMap mMap;
    private boolean NEED_LOG = Constants.NEED_LOG;
    private String TAG = "MapsActivity";
    private MapsViewModel mapsViewModel;
    private MapsViewModelDB mapsViewModelDB;
    private AppSettings appSettings;
    private Marker marker =null;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mapsViewModel= ViewModelProviders.of(this).get(MapsViewModel.class);
        mapsViewModelDB= ViewModelProviders.of(this).get(MapsViewModelDB.class);
        mapsViewModelDB.getDone().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        resultReciever = new PlaceResultReciever(null);
        appSettings=new AppSettings(getBaseContext());
        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(marker !=null) mapsViewModelDB.insert(marker,mMap.getCameraPosition());
                else  Snackbar.make(mapFragment.getView(), getString(R.string.no_place_selected), Snackbar.LENGTH_LONG).show();
            }
        });
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        setupState();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(marker !=null) {
            mapsViewModel.setMarkerState(marker);
            appSettings.setLastMarkerState(marker);
        }
        appSettings.setCameraState(mMap.getCameraPosition(),marker);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void setupState(){
        if(appSettings.isHaveCameraLastState())
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(appSettings.getCameraLastPosition()));
        if(mapsViewModel.isHaveMarker())
            createMarkerNew(mapsViewModel.getdLatitude(),mapsViewModel.getdLongtitude(),mapsViewModel.getsMarkerTitle());
    }


    public class PlaceResultReciever extends ResultReceiver{


    public PlaceResultReciever(Handler handler) {
            super(handler);
        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if(resultCode== Constants.SUCCESS_RESULT){
                final Address address=resultData.getParcelable(Constants.RESULT_ADDRESS);
                if(NEED_LOG){
                    Log.d(TAG,"onReceiveResult");
                    Log.d(TAG,"address="+address.getAddressLine(0));
                    Log.d(TAG,"getLocale="+address.getLocale());
                    Log.d(TAG,"city="+address.getLocality());
                    Log.d(TAG,"state="+address.getAdminArea());
                    Log.d(TAG,"country="+address.getCountryName());
                    Log.d(TAG,"postalCode="+address.getPostalCode());
                    Log.d(TAG,"knownName="+address.getFeatureName());
                    Log.d(TAG,"getSubAdminArea="+address.getSubAdminArea());
                    Log.d(TAG,"getSubLocality="+address.getSubLocality());
                } runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String sPlace=getPlaceName(address);
                        if(sPlace==null)return;
                        createOrCorrectingMarkerNew(address.getLatitude(),address.getLongitude(),sPlace);
                        mapsViewModel.setMarkerState(marker);
                        Snackbar.make(mapFragment.getView(), mapsViewModel.getsMarkerTitle(), Snackbar.LENGTH_LONG).show();

                    }
                });
            }else {
                if(NEED_LOG) Log.d(TAG,"onReceiveResult ERROR"+resultData.getString(Constants.RESULT_MESSAGE));
                Snackbar.make(mapFragment.getView(), getString(R.string.place_not_found), Snackbar.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        double dLatitude=latLng.latitude;
        double dLongitude=latLng.longitude;
        if(NEED_LOG) Log.d(TAG,"onMapClick latitude="+dLatitude+", longitude="+dLongitude);
        startGeoService(dLatitude,dLongitude);
    }


    public void createOrCorrectingMarkerNew(double dLatitude,double dLongtitude,String sTitle){
        if(marker !=null){
            LatLng latLng=new LatLng(dLatitude,dLongtitude);
            if(latLng==null)return;
            marker.setPosition(latLng);
            marker.setTitle(sTitle);
            return;
        }
        createMarkerNew(dLatitude,dLongtitude,sTitle);
    }

    public void createMarkerNew(double dLatitude,double dLongtitude,String sTitle){
        LatLng latLng=new LatLng(dLatitude,dLongtitude);
        if(latLng==null)return;
        marker =mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .alpha(0.7f)
                .zIndex(1.0f)
                .draggable(true));
       if(sTitle!=null)marker.setTitle(sTitle);
    }

    private void startGeoService(double dLatitude,double dLongtitude){
        Intent intent=new Intent(this,GeoService.class);
        intent.putExtra(Constants.LOCATION_LATITUDE_DATA,dLatitude);
        intent.putExtra(Constants.LOCATION_LONGITUDE_DATA,dLongtitude);
        intent.putExtra(Constants.RECEIVER,resultReciever);
        startService(intent);
    }

    private String getPlaceName(Address address){
        if(address.getLocality()!=null)return address.getLocality();
        if(address.getAdminArea()!=null)return address.getAdminArea();
        if(address.getCountryName()!=null)return address.getCountryName();
        return null;
    }
}
