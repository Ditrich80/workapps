package com.example.ledbox.weather.weathers;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ledbox.weather.Constants;
import com.example.ledbox.weather.R;

import java.util.List;
import retrofit2.Callback;


public class WeatherListFragment extends Fragment implements AdapterWeatherListener{
    private WeatherAPI.ApiInterface api;
    private boolean NEED_LOG = Constants.NEED_LOG;
    private String TAG = "MapsActivity";
    private AdapterRecyclerWeather adapterRecyclerWeather;
    private RecyclerView recyclerView;
    private WeatherViewModel weatherViewModel;
    private WeatherViewModelDB weatherViewModelDB;
    private Context context;
    private Intent intent;

    public static WeatherListFragment newInstance(){
        return new WeatherListFragment();
    }
    @Override
    public void onAttach(Context context) {
        if(NEED_LOG)Log.d(TAG,"WeatherListFragment onAttach");
        super.onAttach(context);
        this.context=context;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if(NEED_LOG)Log.d(TAG,"WeatherListFragment onCreate");
        super.onCreate(savedInstanceState);
        adapterRecyclerWeather=new AdapterRecyclerWeather(context);
        weatherViewModel= ViewModelProviders.of(this).get(WeatherViewModel.class);
        weatherViewModelDB= ViewModelProviders.of(this).get(WeatherViewModelDB.class);
        if(!weatherViewModel.isHaveInfo()){
            intent=getActivity().getIntent();
            setData(intent);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(NEED_LOG) Log.d(TAG,"WeatherListFragment onCreateView");
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        api = WeatherAPI.getClient().create(WeatherAPI.ApiInterface.class);
        recyclerView=view.findViewById(R.id.recyclerWeatherItems);
        recyclerView.setAdapter(adapterRecyclerWeather);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        ItemTouchHelper.Callback callbackBL = new ItemTouchHelperCallbackWeather(this);
        ItemTouchHelper mItemTouchHelperBL = new ItemTouchHelper(callbackBL);
        mItemTouchHelperBL.attachToRecyclerView(recyclerView);
        init();
        return view;
    }

    private void init(){
        weatherViewModelDB.getWeatherItems().observe(this, new Observer<List<WeatherItem>>() {
            @Override
            public void onChanged(@Nullable List<WeatherItem> weatherItems) {
                adapterRecyclerWeather.setWeatherDays(weatherItems);
            }
        });
        weatherViewModel.getsPlace().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                ((WeatherActivity)getActivity()).getSupportActionBar().setTitle(s.toUpperCase());
            }
        });
    }


    public void setData(Intent incomingIntent){
        weatherViewModel.setData(incomingIntent);
        weatherViewModelDB.setData(incomingIntent);
    }

    @Override
    public void onAdapterDeleteAction(int pos) {
        final WeatherItem weatherItem=adapterRecyclerWeather.getItemByPos(pos);
        AlertDialog.Builder builderDel = new AlertDialog.Builder(context);
        builderDel.setMessage(getString(R.string.delete_item)+" \""
                +adapterRecyclerWeather.getFormattedDate(weatherItem.insert_time)
                +"\"?");
        builderDel.setIcon(R.drawable.ic_delete_black_24dp);
        builderDel.setTitle(getString(R.string.delete_item));
        builderDel.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                weatherViewModelDB.delWeatherItem(weatherItem);
            }
        });
        builderDel.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                adapterRecyclerWeather.notifyDataSetChanged();
            }
        });
        builderDel.setCancelable(true);
        AlertDialog alertDialog = builderDel.create();
        alertDialog.show();
    }

    public void getWeather() {
        if(weatherViewModelDB.getnParentID()==-1)return;
        double dLatitude=weatherViewModel.getdLatitude();
        double dLongtitude=weatherViewModel.getdLongtitude();
        String sUnits = "metric";
        String app_ID = WeatherAPI.KEY;
        String sLanguage = getResources().getConfiguration().locale.getLanguage();
        if(NEED_LOG) Log.d(TAG, "getWeather LAT="+dLatitude +",  LONG="+dLongtitude);
        api.getToday(dLatitude, dLongtitude, sUnits, app_ID,sLanguage).enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(retrofit2.Call<WeatherDay> call, retrofit2.Response<WeatherDay> response) {
                if(NEED_LOG) Log.d(TAG, "getWeather onResponse");
                if (!response.isSuccessful()) {
                    if(NEED_LOG) Log.d(TAG, "getWeather onResponse response was not Successful");
                    return;
                }
                if(response==null){
                    if(NEED_LOG) Log.d(TAG, "getWeather onResponse response==null");
                    return;
                }
                WeatherItem weatherItem =new WeatherItem(response.body(),weatherViewModelDB.getnParentID(),System.currentTimeMillis());
                if(NEED_LOG)response.body().makeLog();
                weatherViewModelDB.insert(weatherItem);

            }

            @Override
            public void onFailure(retrofit2.Call<WeatherDay> call, Throwable t) {
                if(NEED_LOG) Log.e(TAG, "getWeather onFailure "+t.toString());
            }
        });
    }

}
