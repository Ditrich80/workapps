package com.example.ledbox.weather.places;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.ledbox.weather.WeatherDatabase;

import java.util.List;

/**
 * Created by Ledbox on 23.08.2018.
 */

public class PlacesViewModel extends AndroidViewModel {
    private PlacesDao placesDao;
    private LiveData<List<Place>>placeLiveData;

    public PlacesViewModel(@NonNull Application application) {
        super(application);
        placesDao= WeatherDatabase.getDatabase(application).placesDao();
        placeLiveData=placesDao.getAllPlaces();
    }

    public LiveData<List<Place>> getPlaceLiveData() {
        return placeLiveData;
    }



    public void delPlaceByID(int nID){
        AsyncDeleter asyncDeleter=new AsyncDeleter();
        asyncDeleter.execute(nID);
    }

    private class AsyncDeleter extends AsyncTask<Integer,Integer,Boolean> {

        @Override
        protected Boolean doInBackground(Integer... nIDs) {
            Place place=placesDao.findPlaceByID(nIDs[0]);
            placesDao.deletePlaceByName(place.sName);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            placeLiveData=placesDao.getAllPlaces();
        }
    }
}
