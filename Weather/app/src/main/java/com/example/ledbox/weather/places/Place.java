package com.example.ledbox.weather.places;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ledbox on 23.08.2018.
 */
@Entity(tableName = "places", indices = {@Index(value = "name", unique = true)})
public class Place {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "p_id")
    public int id;

    @ColumnInfo(name = "name")
    @NotNull
    public String sName;

    @ColumnInfo(name = "latitude")
    @NotNull
    public double dLatitude;

    @ColumnInfo(name = "longtitude")
    @NotNull
    public double dLongtitude;

    @ColumnInfo(name = "creation_time")
    @NotNull
    public long lCreationTime;

    @ColumnInfo(name = "last_update")
    public long lLastUpdateDate=0;

    @ColumnInfo(name = "items_coll")
    public int nItemsColl=0;

    @ColumnInfo(name = "camera_latitude")
    @NotNull
    public double dCamLatitude;

    @ColumnInfo(name = "camera_longtitude")
    @NotNull
    public double dCamLongtitude;

    @ColumnInfo(name = "camera_zoom")
    @NotNull
    public float fCamZoom;

    @ColumnInfo(name = "camera_bearing")
    @NotNull
    public float fCamBearing;

    @ColumnInfo(name = "camera_tilt")
    @NotNull
    public float fCamTilt;

    public Place() {
    }

    public Place(Marker marker, CameraPosition cameraPosition, @NotNull long lCreationTime) {
        this.sName = marker.getTitle();
        this.dLatitude = marker.getPosition().latitude;
        this.dLongtitude = marker.getPosition().longitude;
        this.lCreationTime = lCreationTime;
        this.dCamLatitude = cameraPosition.target.latitude;
        this.dCamLongtitude = cameraPosition.target.longitude;
        this.fCamZoom = cameraPosition.zoom;
        this.fCamBearing = cameraPosition.bearing;
        this.fCamTilt = cameraPosition.tilt;
    }

    public CameraPosition getCameraPosition(){
        LatLng latLng=new LatLng(dCamLatitude,dCamLongtitude);
        return new CameraPosition(latLng,fCamZoom,fCamTilt,fCamBearing);
    }

}
