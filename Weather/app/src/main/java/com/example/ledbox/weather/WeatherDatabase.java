package com.example.ledbox.weather;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.ledbox.weather.places.Place;
import com.example.ledbox.weather.places.PlacesDao;
import com.example.ledbox.weather.weathers.WeatherDao;
import com.example.ledbox.weather.weathers.WeatherItem;


@Database(entities = {Place.class, WeatherItem.class}, version = 1, exportSchema = false)
public abstract class WeatherDatabase extends RoomDatabase {
    private static WeatherDatabase instance;
    private static final String DB_NAME = "weathers.db";
    public abstract WeatherDao weatherDao();
    public abstract PlacesDao placesDao();
    public static WeatherDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (WeatherDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            WeatherDatabase.class, DB_NAME).build();
                }
            }
        }
        return instance;
    }
}
