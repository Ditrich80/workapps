package com.example.ledbox.weather.weathers;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Ledbox on 23.08.2018.
 */
@Dao
public interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(WeatherItem weatherItem);

    @Query("DELETE FROM weather_data WHERE w_id = :nID")
    void deleteWeatherItemByID(int nID);

    @Query("SELECT * FROM weather_data WHERE parentid = :nID ORDER BY insert_time DESC")
    LiveData<List<WeatherItem>> getAllWeathersByID(int nID);

    @Query("SELECT * FROM weather_data WHERE parentid = :nID ORDER BY insert_time DESC")
    List<WeatherItem> getAllWeathersByIDNoLiveData(int nID);
}
