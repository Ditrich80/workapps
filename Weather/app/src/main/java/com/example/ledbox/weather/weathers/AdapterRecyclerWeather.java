package com.example.ledbox.weather.weathers;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ledbox.weather.databinding.ItemRecyclerWeatherBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Ledbox on 23.08.2018.
 */

public class AdapterRecyclerWeather extends RecyclerView.Adapter<AdapterRecyclerWeather.WeatherViewHolder> {
    private LayoutInflater layoutInflater;
    private List<WeatherItem>weatherDays;
    public AdapterRecyclerWeather(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRecyclerWeatherBinding binding=ItemRecyclerWeatherBinding.inflate(layoutInflater,parent,false);
        return new WeatherViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        if(weatherDays==null||weatherDays.size()==0)return;
        final WeatherItem weatherItem=weatherDays.get(position);
        holder.binding.setWeather(weatherItem);


    }
    public WeatherItem getItemByPos(int pos){return weatherDays.get(pos);}

    @Override
    public int getItemCount() {
        return weatherDays==null?0:weatherDays.size();
    }

    static class WeatherViewHolder extends RecyclerView.ViewHolder{
        ItemRecyclerWeatherBinding binding;
        public WeatherViewHolder(View itemView) {
            super(itemView);
            binding= DataBindingUtil.bind(itemView);
        }
    }

    public void setWeatherDays(List<WeatherItem> weatherDays) {
        this.weatherDays = weatherDays;
        notifyDataSetChanged();
    }


    public static String getFormattedDate(long fTime){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("d MMMM yyyy, EEEE, k:m");
        return simpleDateFormat.format(new Date(fTime));
    }
}
